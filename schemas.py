from pydantic import BaseModel


class BaseRecipe(BaseModel):
    title: str
    preparation_time: int
    views: int


class RecipeOut(BaseRecipe):
    class Config:
        orm_mode = True
