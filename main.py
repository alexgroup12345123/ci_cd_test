import os
from typing import List

from fastapi import FastAPI, HTTPException
from sqlalchemy import desc
from sqlalchemy.future import select

import ci_cd_test.models as models
import ci_cd_test.schemas as schemas

from .database import engine, session

app = FastAPI()


@app.on_event("startup")
async def startup():
    db_file = "recipes_app.db"
    if not os.path.exists(db_file):
        async with engine.begin() as conn:
            await conn.run_sync(models.Base.metadata.create_all)
        async with models.session as start_session:
            await models.insert_ingridietns(start_session)
        async with models.session as start_session:
            await models.insert_recipes(start_session)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()

    # Удаляем файл базы данных при завершении приложения
    db_file = "recipes_app.db"
    if os.path.exists(db_file):
        os.remove(db_file)


@app.get("/")
async def read_main():
    return {"msg": "Hello World"}


# Получение списка рецептов
@app.get("/recipes/", response_model=List[schemas.RecipeOut])
async def get_recipes() -> List[models.Recipe]:
    result_recipes = await session.execute(
        select(models.Recipe).order_by(
            desc(models.Recipe.views), models.Recipe.preparation_time
        )
    )
    result = result_recipes.fetchall()
    return [recipe for recipe, in result]


# Получение деталей рецепта
@app.get("/recipes/{recipe_id}")
async def get_recipe(recipe_id: int):
    recipe = await session.execute(
        select(models.Recipe).filter(models.Recipe.id == recipe_id)
    )
    result = recipe.scalar_one_or_none()
    if result:
        result.increment_views()

        rec_id = result.id
        ing_result = await session.execute(
            select(models.Ingredient).filter(models.Recipe.id == rec_id)
        )
        ing_tuple = ing_result.fetchall()
        ingredients = [ingredient.name for ingredient, in ing_tuple]

    if result is None:
        raise HTTPException(status_code=404, detail="Recipe not found")

    return {"recipe": result, "ingredients": ingredients}
