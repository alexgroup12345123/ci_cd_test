# type: ignore
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base, async_session


class Recipe(Base):
    __tablename__ = "Recipe"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True, nullable=False)
    preparation_time = Column(Integer, nullable=False)
    views = Column(Integer, default=0, nullable=False)
    text = Column(String, nullable=False)

    def increment_views(self):
        self.views += 1


class Ingredient(Base):
    __tablename__ = "Ingredient"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, nullable=False)
    recipe_id = Column(Integer, ForeignKey("Recipe.id"))
    recipe = relationship("Recipe", backref="Ingredient")


async def insert_recipes(db: async_session):
    data_recipes = [
        {
            "title": "Pasta",
            "preparation_time": 60,  # 1 час = 60 минут
            "views": 1,
            "text": "Pasta is a type of Italian food made from durum wheat "
            "semolina or other grains, mixed with"
            " water to form a dough, which is typically shaped into "
            "various forms such as noodles, macaroni, "
            "spaghetti, or shells",
        },
        {
            "title": "Pizza",
            "preparation_time": 45,  # 45 минут
            "views": 2,
            "text": "Pizza is an Italian dish consisting of a yeasted"
            " flatbread typically topped with tomato "
            "sauce and cheese and baked in an oven. It is commonly"
            " topped with a variety of ingredients "
            "including meats, vegetables, and condiments.",
        },
        {
            "title": "Burger",
            "preparation_time": 20,  # 20 минут
            "views": 3,
            "text": "A burger is a sandwich consisting of one or more"
            " cooked patties of ground meat, usually beef, "
            "placed inside a sliced bread roll or bun. The patty may"
            " be pan-fried, grilled, or flame-broiled.",
        },
        {
            "title": "Sushi",
            "preparation_time": 30,  # 30 минут
            "views": 4,
            "text": "Sushi is a Japanese dish consisting of vinegared"
            " rice combined with various ingredients such "
            "as seafood, vegetables, and occasionally tropical fruits."
            " It is often served with pickled ginger, "
            "soy sauce, and wasabi.",
        },
        {
            "title": "Salad",
            "preparation_time": 10,  # 10 минут
            "views": 2,
            "text": "A salad is a dish consisting of a mixture of "
            "small pieces of food, typically vegetables or fruit. "
            "It is often dressed with a sauce or vinaigrette and "
            "can be served as an appetizer or a main "
            "course.",
        },
    ]
    for recipe in data_recipes:
        new_recipe = Recipe(
            title=recipe["title"],
            preparation_time=recipe["preparation_time"],
            views=recipe["views"],
            text=recipe["text"],
        )
        db.add(new_recipe)
    try:
        await db.commit()
    except Exception as e:
        await db.rollback()
        raise e
    finally:
        await db.close()


async def insert_ingridietns(db: async_session):
    all_ingredients = [
        [
            "Spaghetti (or other pasta)",
            "Tomatoes (for sauce)",
            "Garlic",
            "Onion",
            "Olive oil",
            "Basil (for pesto sauce)",
            "Parmesan cheese (for pesto sauce)",
            "Salt",
            "Pepper",
            "Parsley (for garnish)",
        ],
        [
            "Pizza dough",
            "Tomato sauce",
            "Mozzarella cheese",
            "Pepperoni slices",
            "Mushrooms",
            "Green bell peppers",
            "Onions",
            "Black olives",
            "Olive oil",
            "Basil leaves",
            "Oregano",
            "Salt",
            "Crushed red pepper flakes (optional for heat)",
        ],
        [
            "Burger bun",
            "Ground beef patty",
            "Lettuce",
            "Tomato slice",
            "Onion slice",
            "Cheese slice (optional)",
            "Pickles",
            "Ketchup",
            "Mustard",
            "Mayonnaise",
            "Salt",
            "Pepper",
        ],
        [
            "Sushi rice",
            "Nori seaweed sheets",
            "Fresh fish (e.g., tuna, salmon)",
            "Cucumber",
            "Avocado",
            "Soy sauce",
            "Wasabi",
            "Pickled ginger",
            "Sesame seeds",
            "Rice vinegar",
            "Sugar",
            "Salt",
            "Bamboo sushi rolling mat (for making rolls)",
            "Plastic wrap (for rolling sushi)",
        ],
        [
            "Lettuce (e.g., iceberg, romaine, or mixed greens)",
            "Tomatoes",
            "Cucumbers",
            "Carrots",
            "Bell peppers (e.g., red, green, or yellow)",
            "Red onions",
            "Olives (e.g., black or green)",
            "Feta cheese (optional)",
            "Croutons",
            "Salad dressing (e.g., vinaigrette, ranch, or Caesar)",
            "Olive oil (for homemade dressings)",
            "Balsamic vinegar (for homemade dressings)",
            "Salt",
            "Pepper",
            "Fresh herbs (e.g., basil, parsley, or cilantro, for garnish)",
        ],
    ]
    for i in range(len(all_ingredients)):
        for j in all_ingredients[i]:
            ingridient = Ingredient(name=j, recipe_id=i + 1)
            db.add(ingridient)
    try:
        await db.commit()
    except Exception as e:
        await db.rollback()
        raise e
    finally:
        await db.close()
